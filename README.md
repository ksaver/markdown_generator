# markdown generator
This is a simple markdown generator, to test the creation of a Go web application

It can be deployed successfully locally, by running:
```
$  go build markdown_generator.go
$ ./markdown_generator
```

And open your browser at [localhost:8000](http://127.0.0.1:8080)